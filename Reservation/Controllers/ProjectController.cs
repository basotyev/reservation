﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Reservation.DTO;
using Reservation.Models;
using Reservation.Repositories.Interfaces;

namespace Reservation.Controllers
{
    [Authorize(Roles = Role.moderator)]
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectRepository _projectRepository;
        public ProjectController(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }
        [HttpPost("waitingList")]
        public async Task<IEnumerable<Project>> GetProjectRequests() 
        {
            var request = await _projectRepository.GetAllAsync();
            var result = request.Where(p => p.IsAllowed == false).OrderBy(p => p.Name);
            return result;
        }
        [HttpPost("all")]
        public async Task<IEnumerable<Project>> GetAll()
        {
            var task = await _projectRepository.GetAllAsync();
            return task;
        }
        [HttpPost("allow")]
        public async Task<IActionResult> AllowProject(AllowProjectDTO projectDTO)
        {
            if (!await _projectRepository.IsProjectExists(projectDTO.id))
                return BadRequest("Project does not exists.");
            await _projectRepository.AllowProjectAsync(projectDTO.id);
            return StatusCode(201);
        }
    }
}
