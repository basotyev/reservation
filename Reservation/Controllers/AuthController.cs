﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Reservation.DTO;
using Reservation.Models;
using Reservation.Repositories.Interfaces;

namespace Reservation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthRepository _authRepository;
        private readonly IConfiguration _config;
        public AuthController(IAuthRepository authRepository, IConfiguration config)
        {
            _authRepository = authRepository;
            _config = config;
        }
        [HttpPost("register")]
        public IActionResult Register(UserDTO userDTO)
        {
            if (_authRepository.IsUserExists(userDTO.Id_user))
                return BadRequest("This idcard already registred.");
            var user = new User
            {
                Id_user = userDTO.Id_user,
                Name = userDTO.name,
                Email = userDTO.Email,
                Role = Role.standart
            };
            var createdUser = _authRepository.Register(user, userDTO.password);
            return StatusCode(201);
        }
        [HttpPost("login")]
        public IActionResult Login(UserLoginDTO userDTO) 
        {
            var user = _authRepository.Login(userDTO.id, userDTO.password);

            if (user == null)
                return Unauthorized();

            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id_user.ToString()),
                new Claim(ClaimTypes.Name, user.Name),
                new Claim(ClaimTypes.Role, user.Role)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:Token").Value));

            var tokenDescription = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),   
                Expires = DateTime.Now.AddHours(3),
                SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature)
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescription);
            return Ok(new
            {
                token = tokenHandler.WriteToken(token)
            });
        }

    }
}
