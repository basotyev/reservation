﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Reservation.Core.DTOs.Booking;
using Reservation.DTO;
using Reservation.Models;
using Reservation.Repositories.Interfaces;

namespace Reservation.Controllers
{
    [Authorize(Roles = Role.standart +"," + Role.moderator)]
    [Route("api/[controller]")]
    [ApiController]
    public class BookingsController : ControllerBase
    {
        private readonly IBookingRepository _bookingRepository;
        private readonly IUserRepository _userRepository;
        public BookingsController(IBookingRepository bookingRepository, IUserRepository userRepository)
        {
            _bookingRepository = bookingRepository;
            _userRepository = userRepository;
        }
        [HttpGet("all")]
        public async Task<IEnumerable<Booking>> GetBookings()
        {
            return await _bookingRepository.GetAllAsync();
        }
        [HttpPost("add")]
        public async Task<IActionResult> AddBooking(AddBookingDTO b)
        {
            Booking nb = new Booking() { Id = Guid.NewGuid(),
                Active = b.Active, CabNumber = b.CabNumber,
                Start = b.Start, End = b.End, Id_user = b.Id_user };
            var result = (await _bookingRepository.AddAsync(nb));
            return StatusCode(201);
        }
        [HttpPost("delete")]
        public async Task<bool> DeleteBooking(DeleteBookingDTO b)
        {
            return await _bookingRepository.DeleteBooking(b);
        }
        [HttpPost("personal")]
        public async Task<IEnumerable<Booking>> GetPersonalBookings(UserBookingsDTO u)
        {
            User user = _userRepository.GetUserById(u.userId);
            return await _bookingRepository.GetPersonalBookings(user);
        }
    }
}
