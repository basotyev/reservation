﻿using AutoMapper;
using Reservation.DTO;
using Reservation.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Reservation.Core.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Booking, AddBookingDTO>();
            CreateMap<Project, ProjectDTO>();
            CreateMap<User, UserDTO>();
        }
    }
}
