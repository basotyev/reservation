﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Reservation.DTO
{
    public class UserDTO
    {
        [Key]
        public int Id_user { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string password { get; set; }
        public string name { get; set; }
    }
}
