﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Reservation.DTO
{
    public class ProjectDTO
    {   
        [Required]
        public string Name { get; set; }
        [Column(TypeName = "text")]
        public string description { get; set; }
        [Required]
        public int owner { get; set; }
    }
}
