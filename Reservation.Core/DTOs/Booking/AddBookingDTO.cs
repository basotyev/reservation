﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reservation.DTO
{
    public class AddBookingDTO
    {
        public int Id_user { get; set; }
        public int CabNumber { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public bool Active { get; set; }

    }
}
