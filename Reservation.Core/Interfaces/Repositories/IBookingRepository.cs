﻿using Reservation.Core.DTOs.Booking;
using Reservation.Core.Interfaces.Repositories;
using Reservation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reservation.Repositories.Interfaces
{
    public interface IBookingRepository : IEntityRepository<Booking>
    {
        Task<IEnumerable<Booking>> GetPersonalBookings(User u);
        Task<bool> DeleteBooking(DeleteBookingDTO b); // That is not deleting but hiding the visability
    }
}
