﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Reservation.Models;
namespace Reservation.Repositories.Interfaces
{
    public interface IUserRepository
    {
        IEnumerable<User> GetUsers();
        User GetUserById(int Id);
        bool AddUser(User u);
    }
}
