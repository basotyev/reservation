﻿using Reservation.Core.Interfaces.Repositories;
using Reservation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reservation.Repositories.Interfaces
{
    public interface IAuthRepository
    {
        User Register(User user, string password);
        User Login(int id, string password);
        bool IsUserExists(int id);
    }
}
