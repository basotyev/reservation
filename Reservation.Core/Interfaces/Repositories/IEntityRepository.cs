﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Reservation.Core.Interfaces.Repositories
{
    public interface IEntityRepository<T> where T : class
    {
        Task<T> GetByIdAsync<V>(V id);
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> AddAsync(T entity);
        Task<bool>UpdateAsync(T entity);
        Task DeleteAsync(T entity);
    }
}
