﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Reservation.Models
{
    public class Project
    {
        public Guid id { get; set; }
        public string Name { get; set; }
        public string description { get; set; }
        public int owner { get; set; }
        public bool IsAllowed { get; set; }
    }
}
