﻿using Microsoft.EntityFrameworkCore;
using Reservation.Core.DTOs.Booking;
using Reservation.Data;
using Reservation.Infrastructure.Repositories;
using Reservation.Models;
using Reservation.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reservation.Repositories
{
    public class BookingRepository : EntityRepository<Booking>, IBookingRepository 
    {
        private readonly DbSet<Booking> _bookings;
        public BookingRepository(DataContext dbContext) : base(dbContext)
        {
            _bookings = dbContext.Bookings;
        }
        public async Task<IEnumerable<Booking>> GetPersonalBookings(User u)
        {
            Task<List<Booking>> task = _bookings.Where(b => b.Active & b.Id_user == u.Id_user).ToListAsync();
            return (IEnumerable<Booking>)await task;
        }
        public async Task<bool> DeleteBooking(DeleteBookingDTO dbdto)
        {
            var b = _bookings.Find(dbdto.booking_id);
            b.Active = false;
            return await UpdateAsync(b);
        }
    }
}
