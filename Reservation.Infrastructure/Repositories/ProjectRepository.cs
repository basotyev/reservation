﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.CompilerServices;
using Reservation.Data;
using Reservation.Infrastructure.Repositories;
using Reservation.Models;
using Reservation.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reservation.Repositories
{
    public class ProjectRepository : EntityRepository<Project>, IProjectRepository
    {
        public readonly DbSet<Project> _projects;
        public ProjectRepository(DataContext dbContext) : base(dbContext)
        {
            _projects = dbContext.Projects;
        }
        public async Task<bool> AllowProjectAsync(Guid id)
        {
            var p = await _projects.FindAsync(id);
            p.IsAllowed = true;
            return await UpdateAsync(p);
        }
        public async Task<bool> IsProjectExists(Guid id)
        {
            return await _projects.AnyAsync(p => p.id == id);
        }

        public async Task<IEnumerable<Project>> GetMyProjects(User u)
        {
            Task<List<Project>> task = _projects.Where(p => p.owner == u.Id_user && p.IsAllowed == true).ToListAsync();
            return (IEnumerable<Project>)await task;
        }
    }
}
