﻿using Reservation.Data;
using Reservation.Models;
using Reservation.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reservation.Repositories 
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext _dbContext;
        public UserRepository(DataContext dbContext) 
        {
            _dbContext = dbContext;
        }
        public bool AddUser(User u)
        {
            _dbContext.Users.Add(u);
            return _dbContext.SaveChanges() > 0;
        }

        public User GetUserById(int Id)
        {
            return _dbContext.Users.Find(Id);
        }

        public IEnumerable<User> GetUsers()
        {
            return _dbContext.Users.OrderByDescending(u=>u.Id_user);
        }
    }
}
